#!/usr/bin/env python3

from flask import Flask, render_template, session, abort
from providers.facebook import facebook
from providers.google import google
import boto3
import random
import string

app = Flask(__name__)
app.register_blueprint(facebook, url_prefix='/facebook')
#app.config.from_object('yourapplication.default_settings')
app.config.from_envvar('COGNITO_REF_APP_SETTINGS')
app.secret_key = app.config['SECRET_KEY']


@app.route("/")
def index():
    # Ripped from https://github.com/googleplus/gplus-quickstart-python/blob/master/signin.py
    state = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(32))
    session['state'] = state

    if app.config.get('FACEBOOK', None):
        facebook_url = app.config['FACEBOOK']['OAUTH_URL'].format(appid=app.config['FACEBOOK']['CLIENT_ID'],
                                                                  redirect=app.config['FACEBOOK']['REDIRECT_URL'],
                                                                  state=state)
    else:
        facebook_url = None

    if app.config.get('GOOGLE', None):
        google_url = None
    else:
        google_url = app.config.get('GOOGLE', None)

    return render_template('index.html', facebook_url=facebook_url, google_url=google_url)


@app.route("/cognito")
def cognito():
    client = boto3.client('cognito-identity')

    if session.get('FACEBOOK_ACCESS_TOKEN'):
        access_token = session['FACEBOOK_ACCESS_TOKEN']

        response = client.get_id(AccountId=app.config['AWS']['COGNITO_ACCOUNT_ID'],
                                 IdentityPoolId=app.config['AWS']['IDENTITY_POOL_ID'],
                                 Logins={'graph.facebook.com': access_token})

        identity = response['IdentityId']

        response = client.get_credentials_for_identity(IdentityId=identity,
                                                       Logins={'graph.facebook.com': access_token})

        credentials = response['Credentials']

        s3_client = boto3.client('s3',
                                 aws_access_key_id=credentials['AccessKeyId'],
                                 aws_secret_access_key=credentials['SecretKey'],
                                 aws_session_token=credentials['SessionToken'])

        try:
            buckets = s3_client.list_buckets()
        except client.exceptions.ClientError:
            buckets = None

        return render_template("cognito.html", identity=identity, credentials=credentials, buckets=buckets)
    else:
        abort(401)


if __name__ == '__main__':
    pass
