from flask import Blueprint, render_template, request, current_app, session, abort
import requests

facebook = Blueprint('facebook', __name__,
                     template_folder='templates')


@facebook.route('/login')
def login():
    if session['STATE'] != request.args.state:
        abort(401)

    url = current_app.config['FACEBOOK']['ACCESS_TOKEN_URL'].format(
        client_id=current_app.config['FACEBOOK']['CLIENT_ID'],
        redirect_url=current_app.config['FACEBOOK']['REDIRECT_URL'],
        client_secret=current_app.config['FACEBOOK']['CLIENT_SECRET'],
        code=request.args.get('code', None))

    r = requests.get(url).json()
    session['FACEBOOK_ACCESS_TOKEN'] = r.get('access_token', None)

    return render_template("login.html", token=session['FACEBOOK_ACCESS_TOKEN'])
