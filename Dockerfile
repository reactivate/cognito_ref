FROM debian:latest
EXPOSE 5000

RUN apt-get update && apt-get install -y python3 \
                                         python3-pip

COPY requirements.txt /tmp/
RUN pip3 install -r /tmp/requirements.txt

RUN mkdir /opt/service
COPY . /opt/service

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
ENV COGNITO_REF_APP_SETTINGS=/opt/service/settings.py
ENV FLASK_APP=/opt/service/src/app.py
ENV FLASK_DEBUG=1
WORKDIR /opt/service/src

CMD ["flask", "run", "--host=0.0.0.0"]