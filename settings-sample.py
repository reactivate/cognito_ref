SECRET_KEY = ""
AWS = {
    "COGNITO_ACCOUNT_ID": "FILL-ME-IN",
    "IDENTITY_POOL_ID": "FILL-ME-IN",

}
FACEBOOK = {
    "CLIENT_ID": "FILL-ME-IN",
    "CLIENT_SECRET": "FILL-ME-IN",
    "ACCESS_TOKEN_URL": "https://graph.facebook.com/v2.10/oauth/access_token?client_id={client_id}&redirect_uri={redirect_url}&client_secret={client_secret}&code={code}",
    "REDIRECT_URL": "http://localhost:5000/facebook/login",
    "OAUTH_URL": "https://www.facebook.com/v2.10/dialog/oauth?state={state}&client_id={appid}&redirect_uri={redirect}",
}